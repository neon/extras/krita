Source: krita
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Pino Toscano <pino@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               gettext,
               kf6-extra-cmake-modules,
               libavcodec-dev,
               libavdevice-dev,
               libavfilter-dev,
               libavformat-dev,
               libavutil-dev,
               libboost-system-dev,
               libcurl4-gnutls-dev | libcurl-dev,
               libeigen3-dev,
               libexiv2-dev,
               libfftw3-dev,
               libfontconfig-dev,
               libgif-dev,
               libgsl-dev,
               libharfbuzz-dev,
               libheif-dev,
               libimmer-dev,
               libjpeg-dev,
               libjxl-dev,
               libkf5archive-dev,
               libkf5completion-dev,
               libkf5config-dev,
               libkf5coreaddons-dev,
               libkf5crash-dev,
               libkf5guiaddons-dev,
               libkf5i18n-dev,
               libkf5itemmodels-dev,
               libkf5itemviews-dev,
               libkf5kdcraw-dev,
               libkf5kio-dev,
               libkf5widgetsaddons-dev,
               libkf5windowsystem-dev,
               libkseexpr-dev,
               liblager-dev,
               liblcms2-dev,
               libmlt++-dev,
               libmlt-dev,
               libmypaint-dev,
               libopencolorio-dev,
               libopenexr-dev,
               libopenjp2-7-dev,
               libpng-dev,
               libpoppler-qt5-dev,
               libpostproc-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libquazip5-dev,
               libraqm-dev,
               libraw-dev,
               libsdl2-dev,
               libswresample-dev,
               libswscale-dev,
               libtiff-dev,
               libturbojpeg0-dev,
               libunibreak-dev,
               libwebp-dev,
               libxcb-util0-dev,
               libxcb1-dev,
               libxi-dev,
               libxsimd-dev,
               libzug-dev,
               pkg-config,
               pkg-kde-tools,
               pyqt5-dev,
               pyqt5-dev-tools,
               python3-sip-dev,
               qtbase5-dev,
               qtbase5-private-dev,
               qtdeclarative5-dev,
               qtmultimedia5-dev,
               qtquickcontrols2-5-dev,
               vc-dev [!armhf !arm64],
               xtl-dev,
               zlib1g-dev
Standards-Version: 4.6.2
Homepage: https://krita.org/
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/kde-std/krita.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/kde-std/krita.git

Package: krita
Section: graphics
Architecture: any
Depends: krita-data (>= ${source:Version}),
         krita-l10n,
         krita-python3
         qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: colord
Breaks: krita-data (<< 1:2.4)
Replaces: krita-data (<< 1:2.4)
Description: pixel-based image manipulation program
 Krita is a creative application for raster images. Whether you want to create
 from scratch or work with existing images, Krita is for you. You can work with
 photos or scanned images, or start with a blank slate. Krita supports most
 graphics tablets out of the box.
 .
 Krita is different from other graphics design programs in that it has
 pluggable brush engines, some supporting brush resources like Gimp brush files,
 others offering sophisticated simulation of real brushes, and others again
 offering color mixing and image deformations. Moreover, Krita has full
 support for graphics tablets, including such features as pressure, tilt and
 rate, making it a great choice for artists. There are easy to use tools for
 drawing lines, ellipses and rectangles, and the freehand tool is supported by
 pluggable "drawing assistants" that help you draw shapes that still have a
 freehand feeling to them.
 .
 This package is part of the Calligra Suite.

Package: krita-data
Architecture: all
Section: graphics
Depends: ${misc:Depends}
Description: data files for Krita painting program
 This package contains architecture-independent data files for Krita,
 the painting program shipped with the Calligra Suite.
 .
 See the krita package for further information.
 .
 This package is part of the Calligra Suite.

Package: krita-l10n
Architecture: all
Section: localization
Depends: ${misc:Depends}
Description: translations for Krita painting program
 This package contains the translations for Krita, the painting program
 shipped with the Calligra Suite.
 .
 See the krita package for further information.
 .
 This package is part of the Calligra Suite.

Package: krita-python3
Architecture: all
Section: graphics
Depends: ${misc:Depends}
Description: data files for Krita painting program
 This package contains architecture-independent data files for Krita,
 the painting program shipped with the Calligra Suite.
 .
 See the krita package for further information.
 .
 This package is part of the Calligra Suite.
